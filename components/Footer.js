import SocialBlocks from '../elements/SocialBlocks';
import PropTypes from 'prop-types';

const Footer = ({footer}) => {

    return(
        <footer className="footer-container">

            <div className="wrapper">
                <div className="footer-legal">
                    {footer.legal}
                </div>
                <SocialBlocks socials={footer.socialLink}/>
            </div>
        </footer>
    )
}

Footer.propTypes = {
    footer: PropTypes.shape({
        legal: PropTypes.string,
        socialLink: PropTypes.arrayOf(PropTypes.object)
    })
}

export default Footer;