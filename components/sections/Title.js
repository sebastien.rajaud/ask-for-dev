import PropTypes from 'prop-types';

const Title = ({content}) => {

    const { htmlTag, text} = content;
    const CustomTag = htmlTag;

    return(
        <CustomTag>{text}</CustomTag>
    )
}

Title.propTypes = {
    content: PropTypes.shape({
        htmlTag: PropTypes.oneOf(['h1', 'h2', 'h3','h4']).isRequired,
        text: PropTypes.string.isRequired
    })
}

export default Title;