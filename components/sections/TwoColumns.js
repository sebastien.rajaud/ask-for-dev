import Image from '../../elements/image';
import BlockButtons from '../../elements/BlockButtons';
import {formatText} from '../../utils/text';
import ReactMarkdown from "react-markdown";
import PropTypes from 'prop-types';


const TwoColumns = ({content}) => {
    const {title, text, picture, buttonLinks, textLeft} = content;

    const CustomTag = content.html_tag;
    const formattedText = formatText(text);

    return (
        <section  id={content.anchor} className="two-columns__section">
            <div className={`two-columns__caption ${textLeft ? '' : 'last'}`}>
                <CustomTag className="two-columns__title">{title}</CustomTag>
                <div className="two-columns__text">
                    <ReactMarkdown source={text} />
                </div>
                {buttonLinks && buttonLinks.length > 0 && <BlockButtons buttons={buttonLinks}/>}
            </div>
            <div className={`two-columns__picture ${textLeft ? 'last' : 'order-before'}`}>
                <Image media={picture}/>
            </div>
        </section>
    )
}

TwoColumns.propTypes = {
    content: PropTypes.shape({
        anchor: PropTypes.string,
        title: PropTypes.string,
        html_tag: PropTypes.string,
        text: PropTypes.string,
        picture: PropTypes.object,
        buttonLinks: PropTypes.arrayOf(PropTypes.object),
        textLeft: PropTypes.boolean
    })

}

export default TwoColumns;