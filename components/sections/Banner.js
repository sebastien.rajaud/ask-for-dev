import LeadForm from '../../elements/LeadForm';
import ReactMarkdown from "react-markdown";
import PropTypes from 'prop-types';


const Banner = ({content}) => {
    let form = null;
    return (
        <section id={content.anchor} className="banner-section">
            <div className="banner__caption">
                <h2 className="banner__title">{content.title}</h2>
                <div className="banner__teasing">
                    <ReactMarkdown source={content.teasing}/>
                </div>
            </div>
            <LeadForm/>
        </section>
    )
}

Banner.propTypes = {
    content: PropTypes.shape({
        anchor: PropTypes.string,
        title: PropTypes.string,
        teasing: PropTypes.string
    })
}


export default Banner;