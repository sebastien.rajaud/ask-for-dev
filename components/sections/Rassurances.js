import Image from '../../elements/image';
import PropTypes from 'prop-types';

const Rassurances = ({content}) => {
    return (

        <section id={content.anchor} className="rassurance__section">
            <h2 className="rassurance__section__title">{content.title}</h2>
            <div className="rassurance__list">
                {
                    content.rassuranceItem.map(item => {
                        return (
                            <div key={item._id} className="rassurance__item">
                                <div className="rassurance__picto">
                                    <Image media={item.picto}/>
                                </div>
                                <h3 className="rassurance__title">{item.title}</h3>
                                <div className="rassurance__description">{item.description}</div>
                            </div>
                        )
                    })
                }
            </div>
        </section>
    )
}

Rassurances.propTypes = {
    content: PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        rassuranceItem: PropTypes.arrayOf(PropTypes.shape({
            picto: PropTypes.object,
            title: PropTypes.string,
            description: PropTypes.string,
        }))
    })
}

export default Rassurances;