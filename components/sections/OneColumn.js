import ReactMarkdown from "react-markdown";
import {formatText} from "../../utils/text";
import PropTypes from 'prop-types';


const OneColumn = ({content}) => {
    const formattedContent = formatText(content.content)

    return (
        <section className="one-column" id={content.anchor}>
            <ReactMarkdown source={content.content} />
        </section>
    )
}

OneColumn.propTypes = {
    content: PropTypes.shape({
        anchor: PropTypes.string,
        content: PropTypes.string
    })
}
export default OneColumn;