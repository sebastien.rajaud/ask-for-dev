import Image from '../../elements/image';
import BlockButtons from "../../elements/BlockButtons";
import ReactMarkdown from "react-markdown";
import PropTypes from 'prop-types';

const Hero = ({content}) => {
    const {title, picture, text, buttonLinks} = content;
    return (
        <section id={content.anchor} className="hero-banner">
            <div className="hero__caption">
                <div className="hero__title">{title}</div>
                <div className="hero__text">
                    <ReactMarkdown source={text}/>
                </div>
                {buttonLinks && buttonLinks.length > 0 && <BlockButtons buttons={buttonLinks}/>}
            </div>
            <div className="hero__picture">
                <Image media={picture}/>
            </div>
        </section>
    )
}

Hero.propTypes = {
    content: PropTypes.shape({
        anchor: PropTypes.string,
        title: PropTypes.string,
        text: PropTypes.string,
        picture : PropTypes.object,
        buttonLinks: PropTypes.arrayOf(PropTypes.object)
    })
}


export default Hero;