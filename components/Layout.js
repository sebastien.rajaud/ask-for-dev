import React from 'react';
import NavBar from './Navbar';
import Footer from './Footer';

import PropTypes from 'prop-types';


const Layout = ({children, global, template}) => {

    return (
        <>
            <NavBar navbar={global.NavBar}/>
            <main className="main-container">
                <div className={`wrapper ${template}`}>
                    {children}
                </div>
            </main>
            <Footer footer={global.Footer}/>
        </>
    )
}

Layout.propTypes = {
    global: PropTypes.object,
    template: PropTypes.string,
    Footer: PropTypes.object
}

export default Layout;