import TwoColumns from './sections/TwoColumns';
import OneColumn from './sections/OneColumn';
import Rassurances from './sections/Rassurances';
import Hero from './sections/Hero';
import Banner from './sections/Banner';
import Title from './sections/Title';
import PropTypes from 'prop-types';

const sectionComponents = {
    "sections.one-column": OneColumn,
    "sections.two-columns": TwoColumns,
    "sections.rassurances": Rassurances,
    "sections.hero": Hero,
    "sections.banner": Banner,
    "sections.title": Title,
};


const Section = ({sectionContent}) => {

    const SectionComponent = sectionComponents.[sectionContent.__component];
    return <SectionComponent content={sectionContent} />

}

const Sections = ({sections}) => {
    return (
        <>
            {
                sections.map(section => {
                    return <Section key={section._id} sectionContent={section} />;
                })
            }
        </>
    )
}

Section.propTypes = {
    sectionContent: PropTypes.object.isRequired
}

Sections.propTypes = {
    sections: PropTypes.arrayOf(PropTypes.object)
}

export default Sections;
