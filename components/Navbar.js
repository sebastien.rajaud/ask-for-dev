import {useState} from 'react';
import Link from 'next/link';
import Image from '../elements/image';
import Button from '../elements/Button';
import {faBars} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import PropTypes from 'prop-types';


const NavBarResp = ({navbar, handleClick}) => {
    return (
        <nav className="nav-responsive">
            <ul>
                {
                    navbar.navLinks.map(l => {
                        return (
                            <li key={l._id}>
                                <Link href={l.linkUrl}>
                                    <a>{l.linkText}</a>
                                </Link>
                            </li>
                        )
                    })
                }
                {navbar.NavButton && <li>
                    <Button button={navbar.NavButton}/>
                </li>}
            </ul>
            <button className="btn-close" onClick={() => handleClick()} aria-label="Close menu">
                <FontAwesomeIcon icon={faTimes}/>
            </button>
        </nav>
    )
}

const Navbar = ({navbar}) => {
    const [navResp, setNavResp] = useState(false);

    const handleClick = () => {
        setNavResp(!navResp)
    }


    return (
        <header className="header-container">
            <div className="wrapper">
                <div className="logo">
                    <Link href="/" >
                        <a title="Ask for dev"  aria-label="Ask for dev" >
                            <Image media={navbar.logo}/>
                        </a>
                    </Link>
                </div>
                <nav className="header__menu">
                    <ul>
                        {
                            navbar.navLinks.map(l => {
                                return (
                                    <li key={l._id}>
                                        <Link href={l.linkUrl}>
                                            <a>{l.linkText}</a>
                                        </Link>
                                    </li>
                                )
                            })
                        }
                        {navbar.NavButton && <li>
                            <Button button={navbar.NavButton}/>
                        </li>}
                    </ul>

                    <button className="btn-responsive" onClick={handleClick}  aria-label="Menu">
                        <FontAwesomeIcon icon={faBars}/>
                    </button>
                </nav>

            </div>
            {navResp && <NavBarResp navbar={navbar} handleClick={handleClick}/>}
        </header>
    )
}

Navbar.propTypes = {
    navbar: PropTypes.shape({
        logo: PropTypes.object,
        navLinks: PropTypes.arrayOf(PropTypes.object),
        NavButton: PropTypes.object
    }),


}


NavBarResp.propTypes = {
    navbar: PropTypes.shape({
        logo: PropTypes.object,
        navLinks: PropTypes.arrayOf(PropTypes.object),
        NavButton: PropTypes.object
    }),
    handleClick: PropTypes.func

}


export default Navbar;