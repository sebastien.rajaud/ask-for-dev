import Button from './Button';

export default {
    title: 'AskForDev/Button',
    component: Button,
    argTypes: {
        // backgroundColor: { control: 'color' },
    },
};


//👇 We create a “template” of how args map to rendering
const Template = (args) => <Button {...args} />;

/*
export const Primary = Template.bind({});
Primary.args = {
    primary: true,
    label: 'Button',
};*/
