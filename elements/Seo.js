import { NextSeo } from "next-seo";
import PropTypes from 'prop-types';

const Seo = ({metaDatas}) => {
    const {metaTitle, metaDescription, robotsIndex} = metaDatas;

    return(
        <NextSeo
            noindex={!robotsIndex}
            title={metaTitle}
            description={metaDescription}

        />
    )
}

Seo.propTypes = {
    metaDatas: PropTypes.shape({
        metaTitle: PropTypes.string,
        metaDescription: PropTypes.string,
        robotsIndex: PropTypes.bool,
    })
}
export default Seo;