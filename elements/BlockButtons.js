import Button from "./Button";
import PropTypes from 'prop-types';

const BlockButtons = ({buttons}) => {

    return(
        <div className="block__buttons">
            {buttons.map((button, i) => {
                return <Button key={i} button={button}/>
            })}
        </div>
    )
}

BlockButtons.propTypes = {
    buttons: PropTypes.arrayOf(PropTypes.shape({
        button: PropTypes.object
    }))
}

export default BlockButtons;