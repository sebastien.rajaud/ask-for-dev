import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';


import {
    faGithub,
    faLinkedinIn,
    faFacebookF,
    faTwitter,
    faInstagram,
    faPinterestP
} from '@fortawesome/free-brands-svg-icons';
import {library} from "@fortawesome/fontawesome-svg-core";
import PropTypes from 'prop-types';


const socialList = {
    facebook: faFacebookF,
    twitter: faTwitter,
    linkedin: faLinkedinIn,
    github: faGithub,
    pinterest: faPinterestP,
    instagram: faInstagram
}

const SocialBlocks = ({socials}) => {
    if (!socials) {
        return null;
    }
    return (
        <nav className="social-block">
            <ul>
                {
                    socials.map((s, i) => {
                        return (
                            <li key={s._id}>
                                <a  href={s.url} target="_blank" rel="noreferrer noopener" title={s.name} aria-label={s.name}>
                                    <FontAwesomeIcon icon={socialList[s.name]}/>
                                </a>
                            </li>
                        )
                    })
                }
            </ul>
        </nav>
    )
}

SocialBlocks.propTypes = {
    socials: PropTypes.arrayOf(PropTypes.shape({
      url: PropTypes.string,
      name: PropTypes.string
    }))

}

export default SocialBlocks;