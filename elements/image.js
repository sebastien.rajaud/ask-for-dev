import {getStrapiMedia} from "../utils/media";
import PropTypes from 'prop-types';

const Image = ({media, className}) => {
    const fullUrl = getStrapiMedia(media.url)
    console.log(media)
    return (
        <>
            <img src={fullUrl}
                 alt={media.alternativeText || ''}
                 title={media.alternativeText || ''}
                 className={className}
                 width={media.width}
                 height={media.height}
            />
        </>
    )
}

Image.propTypes = {
    media: PropTypes.object,
    className: PropTypes.string
}

export default Image;
