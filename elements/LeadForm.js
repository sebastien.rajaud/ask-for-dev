import {Formik, Field, Form, ErrorMessage} from 'formik';
import {postLeadFormDatas} from '../utils/api';
import * as yup from 'yup'

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPaperPlane} from "@fortawesome/free-solid-svg-icons";

const CustomErrorMessage = ({children}) => {
    return <div className="form-error-msg">{children}</div>;
}

const LeadForm = (props) => {

    const LeadSchema = yup.object().shape({
        email: yup.string().email().required('Le champ email est requis'),
        name: yup.string().required('Le champ nom est requis'),
        message: yup.string().required('Allez une ptite baffouille !! ;-)'),
    })

    return (
        <Formik
            initialValues={{
                email: '',
                message: '',
                name: ''
            }}
            validationSchema={LeadSchema}
            onSubmit={async (values, {setSubmitting, setErrors, resetForm}) => {

                try {
                    setErrors({api: null})
                    const response = await postLeadFormDatas(values);
                    if (response._id) {
                        resetForm();
                    }

                } catch (err) {
                    setErrors({api: err.message})
                }
                setSubmitting(false)
            }}
        >
            {
                ({isSubmitting, errors}) => (
                    <Form>
                        <div className={`form__field input`}>
                            <Field type='input' name='name' placeholder='Nom'/>
                            <ErrorMessage
                                name="name"
                                component={CustomErrorMessage}
                            />
                        </div>
                        <div className={`form__field email`}>
                            <Field type='email' name='email' placeholder='Email'/>
                            <ErrorMessage
                                name="email"
                                component={CustomErrorMessage}
                            />
                        </div>
                        <div className={`form__field textarea`}>
                            <Field as='textarea' name='message' placeholder='Message'/>
                            <ErrorMessage
                                name="message"
                                component={CustomErrorMessage}
                            />
                        </div>
                        {errors.api ? <CustomErrorMessage>{errors.api}</CustomErrorMessage> : null}
                        <button type="submit" disabled={isSubmitting} aria-label="Soumettre le formulaire de contact">
                            <FontAwesomeIcon icon={faPaperPlane}/>
                        </button>
                    </Form>
                )
            }

        </Formik>

    )
}

export default LeadForm;