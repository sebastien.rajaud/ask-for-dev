import Link from 'next/link';
import PropTypes from 'prop-types';

const Button = ({button}) => {

    const {buttonText, buttonLink, buttonStyle} = button;

    return (
        <Link href={buttonLink} >
            <a className={`btn btn-${buttonStyle}`}>{buttonText}</a>
        </Link>
    )
}

Button.propTypes = {
    button: PropTypes.shape({
        buttonText: PropTypes.string,
        buttonLink: PropTypes.string,
        buttonStyle: PropTypes.oneOf(['primary', 'secondary'])
    })
}

export default Button;