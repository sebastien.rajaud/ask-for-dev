# *
User-agent: *
Allow: /

# Host
Host: https://www.askfordev.fr

# Sitemaps
Sitemap: https://www.askfordev.fr/sitemap.xml
