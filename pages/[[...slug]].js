import {fetchApi, getPageDatas} from "../utils/api";
import {useRouter} from "next/router";
import ErrorPage from "next/error";
import Seo from "../elements/Seo";
import Sections from "../components/Sections";

const DynamicPage = ({content, metaDatas}) => {

    const router = useRouter();


    if(!router.isFallback && content.length === 0 ) {
        return <ErrorPage statusCode={404} />;
    }

    if(router.isFallback) {
        return <div className="container">Loading...</div>;
    }



    return (
        <>
            <Seo metaDatas={metaDatas} />
            <Sections sections={content} />
        </>
    )
}

export async function getStaticPaths() {
    const pages = await fetchApi('/pages');

    const paths = pages.map(p => {
        return {
            params: {slug: p.slug.split("__")}
        }
    })

    return {
        paths, fallback: true
    };


}

export async function getStaticProps({params, preview = null}) {

    let slug;

    if(!params.slug) {
        slug = '';
    } else {
        slug = params.slug.join('__');
    }


    const datas = await getPageDatas(slug);


    if (datas === null) {
        // Generate a 404
        return { props: {} };
    }

    const {Metas, sections, template} = datas ;
    return {
        props: {
            metaDatas : Metas,
            content: sections,
            template: template
        }
    }
}



export default DynamicPage;