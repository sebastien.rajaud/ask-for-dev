import App from "next/app";
import {getGlobalDatas} from '../utils/api';
// import '../styles/globals.css';
import '../styles/main.scss';
import {DefaultSeo} from 'next-seo';
import ErrorPage from 'next/error';

import Layout from "../components/Layout";
import Head from "next/head";



const MyApp = ({Component, pageProps}) => {

    const {global} = pageProps


    if (global == null) {
        return <ErrorPage statusCode={404}/>;
    }

    return (
        <>
            <Head>
                <link
                    rel="preconnect"
                    href="https://fonts.gstatic.com" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Asap+Condensed:wght@400;500;600;700&display=swap"
                    rel="stylesheet" />
            </Head>
            <DefaultSeo
                title={global.metaDatas.metaTitle}
                description={global.metaDatas.metaDescription}
            />
            <Layout global={global} template={pageProps.template} >
                <Component {...pageProps} />
            </Layout>
        </>
)
}


MyApp.getInitialProps = async (ctx) => {
    const defaultProps = App.getInitialProps(ctx);

    const global = await getGlobalDatas();

    return {...defaultProps, pageProps: {global}};
}

export default MyApp
