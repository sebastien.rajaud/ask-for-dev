
export const formatText = (text) => {
    return text.split('\n\n').map((str, i) => <p key={i}>{str}</p>);
}