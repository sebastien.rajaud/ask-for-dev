export const getStrapiMedia = (url) => {
    if(!url) {
        return null;
    }

    if(url.startsWith('http') || url.startsWith('//')) {
        return url
    }

    return `${process.env.NEXT_PUBLIC_STRAPI_API_URL}${url}`

}