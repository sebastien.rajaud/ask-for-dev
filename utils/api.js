export function getStrapiURL(path) {
    return `${
        process.env.NEXT_PUBLIC_STRAPI_API_URL || 'http://localhost:1337'
    }${path}`
}

export const fetchApi = async (path, options = {}) => {
    const defaultOptions = {
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const mergedOptions = {
        ...defaultOptions,
        ...options,
    }
    const requestUrl = getStrapiURL(path)
    const response = await fetch(requestUrl, mergedOptions)

    if (!response.ok) {
        console.error(response.statusText)
        throw new Error(`An error occured please try again`)
    }
    const data = await response.json()
    return data
}

export const getGlobalDatas = async () => {
    const datas = await fetchApi('/global');

    return datas;
}

export const getPageDatas = async (slug) => {

    const datas = await fetchApi(`/pages?slug=${slug}`);


    if (datas === null) {
        return null;
    }

    return datas[0];
}


export const postLeadFormDatas = async (datas) => {
    const defaultOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "name": datas.name,
            "email": datas.email,
            "message": datas.message
        })
    }

    const requestUrl = getStrapiURL(`/lead-form-submissions`);

    try {
        const response = await fetch(requestUrl, defaultOptions);

        if (!response.ok) {
            throw new Error(`Une erreur est survenue lors de l\'envoi du formulaire`)
        } else {
            const datas = await response.json();
            return datas;

        }

    } catch (err) {
        throw err;
    }


}